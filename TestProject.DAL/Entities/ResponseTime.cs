﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProject.DAL.Entities
{
    [Table("ResponseTime")]
    public class ResponseTime
    {
        public ResponseTime()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.UtcNow;
        }

        [Column("Id")] 
        [Key] 
        public string Id { get; set; }

        [Column("CreationDate")] 
        public DateTime CreationDate { get; set; }
        [Column("Request")]
        public string Request { get; set; }

        [Column("MinTime")]
        public long MinTime { get; set; }

        [Column("MaxTime")]
        public long MaxTime { get; set; }
    }
}
