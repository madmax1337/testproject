﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.DAL.Entities;

namespace TestProject.DAL.Repository.Intefaces
{
    public interface IResponseTimeRepository
    {
        Task<IEnumerable<ResponseTime>> GetAll();
        Task<ResponseTime> GetByName(string request);
        Task<ResponseTime> Insert(ResponseTime entity, bool withPrimary = true);
        Task Update(ResponseTime entity);
    }
}
