﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using TestProject.DAL.Entities;
using TestProject.DAL.Repository.Intefaces;

namespace TestProject.DAL.Repository
{
    public class ResponseTimeRepository: IResponseTimeRepository
    {

        protected readonly string _connectionString;

        public ResponseTimeRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<IEnumerable<ResponseTime>> GetAll()
        {
            var sql = "SELECT * FROM ResponseTime ORDER BY MaxTime DESC";

            using (var db = CreateConnection())
            {
                return await db.QueryAsync<ResponseTime>(sql);
            }
        }

        public async Task<ResponseTime> GetByName(string request)
        {
            var sql = $@"SELECT * FROM ResponseTime WHERE Request = '{request}'";
            using (var db = CreateConnection())
            {
                try
                {
                    (await db.QueryAsync<ResponseTime>(sql)).ToList().FirstOrDefault();
                }
                catch (Exception ex)
                {
                    ;
                }
                return (await db.QueryAsync<ResponseTime>(sql)).ToList().FirstOrDefault();
            }
        }

        public async Task<ResponseTime> Insert(ResponseTime entity, bool withPrimary = true)
        {
            var columnsAttributeName = GetColumnsAttributeName(withPrimary);

            var columnsName = GetColumnsName(withPrimary);

            var stringOfColumns = string.Join(", ", columnsAttributeName.Select(e => e));

            var stringOfParameters = string.Join(", ", columnsAttributeName.Select((e, index) => "@" + columnsName.ElementAt(index)));

            var sql = $"INSERT INTO ResponseTime ({stringOfColumns}) VALUES ({stringOfParameters})";

            using (var db = CreateConnection())
            {
                await db.ExecuteAsync(sql, entity);
                return entity;
            }
        }

        public async Task Update(ResponseTime entity)
        {
            var columnsAttributeName = GetColumnsAttributeName(false);

            var columnsName = GetColumnsName(false);

            var stringOfColumns = string.Join(", ",
                columnsAttributeName.Select((e, index) => $"{e} = @{columnsName.ElementAt(index)}"));

            var attributeNamePrimary = GetPrimaryKeyName(true);

            var namePrimary = GetPrimaryKeyName(false);

            var sql = $"UPDATE ResponseTime SET {stringOfColumns} WHERE {attributeNamePrimary} = @{namePrimary}";

            using (var db = CreateConnection())
            {
                await db.ExecuteAsync(sql, entity);
            }
        }
        protected IEnumerable<string> GetColumnsName(bool withPrimaryKey)
        {
            var result = Enumerable.Empty<string>();

            var properties = typeof(ResponseTime).GetProperties()
                .Where(e => !Attribute.IsDefined(e, typeof(NotMappedAttribute)));

            if (withPrimaryKey) result = properties.Select(e => e.Name);

            if (!withPrimaryKey)
                result = properties.Where(e => !Attribute.IsDefined(e, typeof(KeyAttribute))).Select(e => e.Name);

            return result;
        }

        protected IEnumerable<string> GetColumnsAttributeName(bool withPrimaryKey)
        {
            var result = Enumerable.Empty<string>();

            var properties = typeof(ResponseTime).GetProperties().Where(e => !Attribute.IsDefined(e, typeof(NotMappedAttribute)));

            if (withPrimaryKey) result = properties.Select(e => e.GetCustomAttribute<ColumnAttribute>().Name);

            if (!withPrimaryKey)
                result = properties.Where(e => !Attribute.IsDefined(e, typeof(KeyAttribute)))
                    .Select(e => e.GetCustomAttribute<ColumnAttribute>().Name);

            return result;
        }

        protected string GetPrimaryKeyName(bool attributeName)
        {
            var result = string.Empty;

            var propertyInfos = typeof(ResponseTime).GetProperties().Where(e => Attribute.IsDefined(e, typeof(KeyAttribute)));

            if (attributeName)
                result = propertyInfos.Select(e => e.GetCustomAttribute<ColumnAttribute>().Name).FirstOrDefault();

            if (!attributeName) result = propertyInfos.Select(e => e.Name).FirstOrDefault();

            return result;
        }

        protected IDbConnection CreateConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();

            return connection;
        }
    }
}
