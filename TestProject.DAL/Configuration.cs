﻿using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestProject.DAL.AppDbContext;
using TestProject.DAL.Repository;
using TestProject.DAL.Repository.Intefaces;

namespace TestProject.DAL
{
    public class Configuration
    { 
        public static void Add(IServiceCollection services, string connectionString)
        {
            AddDbContext(services, connectionString);
            AddDependecies(services, connectionString);
        }

        private static void AddDbContext(IServiceCollection services, string connectionString)
        {
            var migrationsAssembly = typeof(Configuration).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<ApplicationDbContext>(
                options => { options.UseSqlServer(connectionString, x => x.MigrationsAssembly(migrationsAssembly)); },
                ServiceLifetime.Transient);
        }

        public static void AddDependecies(IServiceCollection services, string connectionString)
        {
            services.AddScoped<IDbConnection, SqlConnection>(c => new SqlConnection(connectionString));
            services.AddTransient<IResponseTimeRepository, ResponseTimeRepository>();
        }
    }
}
