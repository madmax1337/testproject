﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestProject.BLL.Services.Interfaces;

namespace TestProject.Web.Controllers
{
    public class ResponseTimeController : Controller
    {
        private readonly IResponseTimeService _responseTimeService;

        public ResponseTimeController(IResponseTimeService responseTimeService)
        {
            _responseTimeService = responseTimeService;
        }

        public async Task<IActionResult> GetResponseTime()
        {
            var responseModel = await _responseTimeService.GetAllResponseTime();
            return View(responseModel);
        }
    }
}