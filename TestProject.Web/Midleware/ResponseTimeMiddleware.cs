﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using TestProject.BLL.Models;
using TestProject.BLL.Services.Interfaces;

namespace TestProject.Web.Midleware
{
    public class ResponseTimeMiddleware
    {
        private const string RESPONSE_HEADER_RESPONSE_TIME = "Response-Time-ms";
        private readonly RequestDelegate _next;
        private readonly IResponseTimeService _responseTimeService;

        public ResponseTimeMiddleware(RequestDelegate next, IResponseTimeService responseTimeService) 
        {  
            _next = next;
            _responseTimeService = responseTimeService;
        }  

        public Task InvokeAsync(HttpContext context) 
        {
            var watch = new Stopwatch();  
            watch.Start();

            context.Response.OnStarting(() => {
                watch.Stop();

                ResponseTimeModel responseTimeModel = new ResponseTimeModel();
                responseTimeModel.Request = "https://" + context.Request.Host.Value + context.Request.Path;
                responseTimeModel.Time = watch.ElapsedMilliseconds;

                _responseTimeService.AddTime(responseTimeModel);
				
                context.Response.Headers[RESPONSE_HEADER_RESPONSE_TIME] = watch.ElapsedMilliseconds.ToString();  
                return Task.CompletedTask;  
            });

            return this._next(context);  
        }  
    }
}
