﻿using System.Threading.Tasks;
using TestProject.BLL.Models;

namespace TestProject.BLL.Services.Interfaces
{
    public interface IResponseTimeService
    {
        Task AddTime(ResponseTimeModel model);
        Task<ResponseTimesModel> GetAllResponseTime();
    }
}
