﻿using System.Linq;
using System.Threading.Tasks;
using TestProject.BLL.Models;
using TestProject.BLL.Services.Interfaces;
using TestProject.DAL.Entities;
using TestProject.DAL.Repository.Intefaces;

namespace TestProject.BLL.Services
{
    public class ResponseTimeService : IResponseTimeService
    {
        private readonly IResponseTimeRepository _responseTimeRepository;

        public ResponseTimeService(IResponseTimeRepository responseTimeRepository)
        {
            _responseTimeRepository = responseTimeRepository;
        }

        public async Task<ResponseTimesModel> GetAllResponseTime()
        {
            var queryResult = (await _responseTimeRepository.GetAll()).ToList();
            var responseModel = new ResponseTimesModel();
            queryResult.ForEach(item =>
            {
                responseModel.Items.Add(new ResponseTimesItem()
                {
                    DateCreate = item.CreationDate,
                    Request = item.Request,
                    MinTime = item.MinTime,
                    MaxTime = item.MaxTime
                });
            });

            return (responseModel);
        }

        public async Task AddTime(ResponseTimeModel model)
        {
            var queryResult = await _responseTimeRepository.GetByName(model.Request);
            if (queryResult == null)
            {
                ResponseTime requestTime = new ResponseTime();
                requestTime.Request = model.Request;
                requestTime.MaxTime = model.Time;
                requestTime.MinTime = model.Time;
                await _responseTimeRepository.Insert(requestTime);
            }

            if (queryResult != null)
            {
                await UpdateRecord(model, queryResult);
            }
        }

        private async Task UpdateRecord(ResponseTimeModel model, ResponseTime responseTime)
        {
            if (responseTime.MaxTime < model.Time)
            {
                responseTime.MaxTime = model.Time;
                await _responseTimeRepository.Update(responseTime);
            }

            if (responseTime.MinTime > model.Time)
            {
                responseTime.MinTime = model.Time;
                await _responseTimeRepository.Update(responseTime);
            }
        }
    }
}
