﻿namespace TestProject.BLL.Models
{
    public class ResponseTimeModel
    {
        public string Request { get; set; }
        public long Time { get; set; }
    }
}
