﻿using System;
using System.Collections.Generic;

namespace TestProject.BLL.Models
{
    public class ResponseTimesModel
    {
        public List<ResponseTimesItem> Items { get; set; }

        public ResponseTimesModel()
        {
            Items = new List<ResponseTimesItem>();
        }
    }

    public class ResponseTimesItem
    {
        public DateTime DateCreate { get; set; }
        public string Request { get; set; }
        public long MinTime { get; set; }
        public long MaxTime { get; set; }
    }
}
