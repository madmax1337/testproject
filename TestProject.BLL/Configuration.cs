﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestProject.BLL.Services;
using TestProject.BLL.Services.Interfaces;

namespace TestProject.BLL
{
    public class Configuration
    {
        public static void Add(IServiceCollection services, IConfiguration configuration)
        {
            
            DAL.Configuration.Add(services, configuration.GetConnectionString("DefaultConnection"));
            services.AddTransient<IResponseTimeService, ResponseTimeService>();
        }

    }
}
